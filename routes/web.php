<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//Route::group(['prefix'=>'admin'] ,function(){
//
//
//    //Login Route
//    Route::get('login',['as'=>'admin.login','user'=>'AdminAuth\LoginController@showLoginForm']);
//    Route::post('login',['user'=> 'AdminAuth\LoginController@login']);
//    Route::post('logout',['as'=>'admin.logout','user'=>'AdminAuth\LoginController@logout']);
//
//    //Registrations Route
//
//    Route::get('register',['as'=>'admin.register','user'=>'AdminAuth\RegisterController@showRegistrationForm']);
//    Route::post('register',['user'=> 'AdminAuth\RegisterController@register']);
//
//    //Password Reset Route
//    Route::get('password/reset',['as'=>'admin.password.reset','user'=>'AdminAuth\ForgotPasswordController@showLinkRequestForm']);
//    Route::post('password/email',['as'=>'admin.password.email','user'=>'AdminAuth\ForgotPasswordController@sendResetLinkEmail']);
//    Route::get('password/reset{token}',['as'=>'admin.password.reset.token','user'=>'AdminAuth\ResetPasswordController@showResetForm']);
//    Route::post('password/reset',['user'=>'AdminAuth\ResetPasswordController@reset']);
//});
Route::prefix('admin')->group(function() {
    Route::get('/', 'AdminController@index')->name('admin');
    Route::get('/login', 'AdminAuth\Auth\LoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'AdminAuth\Auth\LoginController@login')->name('admin.login.submit');
    Route::post('/logout', 'AdminAuth\Auth\LoginController@logout')->name('admin.logout');
    Route::get('/password/reset', 'AdminAuth\Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('/password/email', 'AdminAuth\Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::get('/password/reset/{token}', 'AdminAuth\Auth\ResetPasswordController@showResetForm')->name('admin.password.reset');
    Route::post('/password/reset', 'AdminAuth\Auth\ResetPasswordController@reset');

//    $this->get('register', 'AdminAuth\Auth\RegisterController@showRegistrationForm')->name('admin.register');
//    $this->post('register', 'AdminAuth\Auth\RegisterController@register')->name('admin.login.submit');
    Route::get('register', 'AdminAuth\Auth\RegisterController@showRegistrationForm')->name('admin.register');
    Route::post('register', 'AdminAuth\Auth\RegisterController@register')->name('admin.login.submit');


    Route::get('admin', ['as'=>'admin','uses'=>'AdminController@dashboard']);
});