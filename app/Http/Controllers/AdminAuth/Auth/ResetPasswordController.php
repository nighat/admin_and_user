<?php

namespace App\Http\Controllers\AdminAuth\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Auth;
class ResetPasswordController extends Controller
{
    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/admin/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('guest');
//    }

    protected function guard()
    {
        return Auth::guard('admin');
    }

    public function showResetForm(Request $request, $token = null)
    {
        return view('adminAuth.passwords.reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }
}